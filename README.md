REPOSITORIO PUBLICO - TERRAFORM ELK
terraform version
Terraform v0.11.13
+ provider.aws v2.60.0

# Overview
this repository creates the following resources on AWS cloud.
before you may change file elk.tfvars

-   **+ HOW TO RUN**
* TF_LOG=DEBUG terraform apply -var-file elk.tfvars

* ![aws elasticsearch](img/logo-aws-elk.png?raw=true "aws-es")

# RESOURCES:

-   **+ SECURITY GROUP**
	
        aws_security_group.sg_resource: sec group - aws elasticsearch 80,443,9200
	
-   **+ aws ELASTICSEARCH**
	
	aws_elasticsearch_domain.es:
	    kibana_endpoint, elasticsearch_version	
