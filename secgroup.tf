// RESOURCE SECURITY GROUP
resource "aws_security_group" "sg_resource" {
 name        = "sg_${var.enviroment}-${var.domain}"
 description = "sec group - aws elasticsearch 80,443,9200"
 vpc_id      = "${var.vpc_id}"
/* ingress {
   from_port   = 9200
   to_port     = 9200
   protocol    = "tcp"
   //security_groups = ["${var.security_group_access}"]
   cidr_blocks = ["0.0.0.0/0"]
 }*/
 ingress {
   from_port   = 80
   to_port     = 80
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }
 ingress {
   from_port   = 443
   to_port     = 443
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }
 tags {
   Name = "SecurityGroup-elasticsearch"
 }
}
