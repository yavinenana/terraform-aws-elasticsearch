output "ElasticSearch Endpoint: "{
	value="${aws_elasticsearch_domain.es.endpoint}"
}
output "ElasticSearch KIBANA Endpoint: "{
	value="${aws_elasticsearch_domain.es.kibana_endpoint}"
}
