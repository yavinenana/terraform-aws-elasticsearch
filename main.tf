resource "aws_elasticsearch_domain" "es" {
  domain_name           = "${var.domain}"
  elasticsearch_version = "${var.elkversion}"
//  iam_instance_profile = "AWSServiceRoleForAmazonElasticsearchService "

  cluster_config {
    instance_type = "${var.instance}"
    instance_count = 1
  }
  ebs_options{
    ebs_enabled = true
    volume_size = "${var.volsize}"
    volume_type = "${var.voltype}"
//    delete_on_termination = "true"
  }
  
/*  snapshot_options {
    automated_snapshot_start_hour = 23
  }
*/

  vpc_options {
    subnet_ids = ["${var.subnet}"]
    security_group_ids = ["${aws_security_group.sg_resource.id}"]
  }
  tags = {
    Domain = "TestDomain"
    env = "${var.enviroment}"
  }
// ACCESS POLICY 
  access_policies = <<POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "AWS": [
            "*"
          ]
        },
        "Action": [
          "es:*"
        ],
        "Resource": "arn:aws:es:${var.regionaws}:${var.aws_account}:domain/${var.domain}/*"
      }
    ]
  }
POLICY
}
