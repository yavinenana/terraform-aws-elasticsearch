// ------------------elastic-aws
// modify this vars 

domain	= "elkdominio"
elkversion = "7.4"
instance = "t2.small.elasticsearch"
aws_account = "account-id"
volsize = "10"
voltype = "gp2"

regionaws   = "us-west-2"
profileaws  = "default"

subnet = "subnet-id"
vpc_id = "vpc-id"
enviroment = "prod"
